Feature: Manager Viewing All Reimbursements
  As a Manager, I wish to view all Reimbursements

  Scenario: Viewing All Reimbursements
    Given a user is at the Management ERS page
    And a user is of user role manager
    When a requests the information
		Then the user is presented with table of all reimbursements
 