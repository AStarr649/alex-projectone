Feature: Employee Submitting New Reimburstment Request
 	As an employee, I wish to submit a new reimburstment request for approval
  
  Scenario: Successful submission of Request
    Given a user is at the employee ERS main page
    And a user submits a new request form
    When a user inputs the amount
    And a user inputs the description of request
    Then the user is shown a successful submission alert
    
	Scenario: Incorrect submission of Request
		Given a user is at the employee ERS main page
		And a user submits a new request form
		When a user incorrectly inputs the amount
		And a user incorrectly inputs the description of request
		Then the user is shown a failure to submit alert
  
