Feature: User Logging into ERS App
	As an employee, I wish to log in to the ERS App using proper credentials
	
	Scenario Outline: Successful Login to the ERS App
		Given a user is at the home page of ERS
		When a user inputs their user name "<username>"
		And a user inputs their password "<password>"
		And a user submits the information
		But that user is a user of role Employee
		Then the user is redirected to the Employee ERS page
		
		Examples:
			|  username  |  password  |
			|  bigSal		 |  password	|
			
	Scenario Outline: Unsuccessful Login to the ERS App
		Given a user is at the home page of ERS
		When a user inputs their user name "<username>"
		And a user incorrectly inputs their password "<password>"
		And a user submits the information
		Then the user is redirected to the bad login page
		
		Examples:
			|  username  |  password  |
			|	 bigSal		 |	superman	|
			
			Scenario Outline: Successful Login to the ERS App
		Given a user is at the home page of ERS
		When a user inputs their user name "<username>"
		And a user inputs their password "<password>"
		And a user submits the information
		But that user is a user of role Manager
		Then the user is redirected to the Managers ERS page
		
		Examples:
			|  username  |  password  |
			|  hotMike	 |  password1	|
			
	Scenario Outline: Unsuccessful Login to the ERS App
		Given a user is at the home page of ERS
		When a user inputs their user name "<username>"
		And a user incorrectly inputs their password "<password>"
		And a user submits the information
		But that user is a user of role Manager
		Then the user is redirected to the bad login page
		
		Examples:
			|  username  |  password  |
			|	 hotMike	 |	superman	|