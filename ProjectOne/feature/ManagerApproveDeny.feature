Feature: Manager Approves/Denies Reimbursement
  As a Manager, I want to approve or deny any pending Reimbursement request

  Scenario: Manager Approves Reimbursement
    Given a user is at the Management ERS page
    And a user is of user role manager
    And the reimbursement request is in status pending
    When a user selects approve on request
    And a user selects the appropriate type id
    Then the reimbursement request is put into status approved
    And the resolved date of request is set to current date
    And the resolver id is set to the user id

  Scenario: Manager Denies Reimbursement
    Given a user is at the Management ERS page
    And a user is of user role manager
    And the reimbursement request is in status pending
    When a user selects deny on request
    Then the reimbursement request is put into status denied
    And the resolved date of request is set to current date
    And the resolver id is set to the user id
    And the type id is set to rejected
