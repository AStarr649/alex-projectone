package com.example.service;

import com.example.dao.UserDAOImpl;
import com.example.model.User;

public class UserService {

	private UserDAOImpl uDao;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}

	public UserService(UserDAOImpl uDao) {
		super();
		this.uDao = uDao;
	}
	
	public User getUserById(int idNum) {
		return uDao.getUserById(idNum);
	}
	
	public User getUserByNames(String fname, String lname) {
		User user = uDao.getUserByName(fname, lname);
		if(user == null) {
			throw new NullPointerException();
		}
		return user;
	}
	
	public User findUserLogin(String username) {
		return uDao.getUserLogin(username);
	}
	
	public User verifyUserLogin(String username, String password) {
		
		User user = findUserLogin(username);
		if(user.getPassword().equals(password)) {
			return user;
		}
		return null;
	}
}
