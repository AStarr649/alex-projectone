package com.example.service;

import java.util.List;

import com.example.dao.ReimbursDAOImpl;
import com.example.model.Reimburstment;

public class ReimburstService {

	private ReimbursDAOImpl rDao;
	
	public ReimburstService() {
		// TODO Auto-generated constructor stub
	}

	public ReimburstService(ReimbursDAOImpl rDao) {
		super();
		this.rDao = rDao;
	}
	
	public void newRequest(Reimburstment reimburs) {
		rDao.insertNewRequest(reimburs);
	}
	
	public List<Reimburstment> retrieveAllReimbursments(){
		return rDao.getAllRequests();
	}
	
	public List<Reimburstment> getAllRequestById(int idNum){
		return rDao.getRequestsForID(idNum);
	}
	
	public Reimburstment getReimbursRequestById(int requestId) {
		return rDao.getRequestById(requestId);
	}
	
	public void updateRequest(Reimburstment request) {
		rDao.updateRequestStatus(request);
	}
	
	public List<Reimburstment> getFilterRequest(){
		return rDao.getRequestByStatus();
	}
	
	public Double getAvgRequest() {
		return rDao.getAvgRequest();
	}
	
	public Double getMaxRequest(){
		return rDao.getMaxRequest();
	}
}
