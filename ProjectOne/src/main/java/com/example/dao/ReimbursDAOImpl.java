package com.example.dao;


import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.example.model.Reimburstment;

public class ReimbursDAOImpl implements ReimbursDAO {

	private ProjectDBConnection rDb;

	public ReimbursDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursDAOImpl(ProjectDBConnection rDb) {
		super();
		this.rDb = rDb;
	}

	@Override
	public void insertNewRequest(Reimburstment newReimburs) {

		try (Connection con = rDb.getDBConnection()) {

			String sql = "{? = call insert_reimbrequest(?,?,?,1)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.BOOLEAN);
			cs.setBigDecimal(2, BigDecimal.valueOf(newReimburs.getImbursAmnt()));
			cs.setString(3, newReimburs.getComment());
			cs.setInt(4, newReimburs.getSubmiter());
			cs.execute();
			System.out.println(cs.getString(1));

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Reimburstment> getAllRequests() {

		List<Reimburstment> reimbList = new ArrayList<>();

		try (Connection con = rDb.getDBConnection()) {

			String sql = "select * from reimbursementmain";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				if(rs.getDate(4) == null) {
					reimbList.add(new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
							rs.getDouble(2), rs.getDate(3).toString(), "------", rs.getString(5)));
				}else {
				reimbList.add(new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
						rs.getDouble(2), rs.getDate(3).toString(), rs.getDate(4).toString(), rs.getString(5)));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimbList;
	}

	@Override
	public Reimburstment getRequestById(int imbursId) {

		Reimburstment reimburs = new Reimburstment();

		try (Connection con = rDb.getDBConnection()) {

			String sql = "select * from reimbursementmain where imbur_id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, imbursId);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				if(rs.getDate(4) == null) {
					reimburs = new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
							rs.getDouble(2), rs.getDate(3).toString(), "------", rs.getString(5));
				}else {
				reimburs = new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
						rs.getDouble(2), rs.getDate(3).toString(), rs.getDate(4).toString(), rs.getString(5));
				}
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimburs;
	}

	@Override
	public void updateRequestStatus(Reimburstment reimbur) {

		try (Connection con = rDb.getDBConnection()) {

			String sql = "{? = call update_request(?,?,?)}";
			CallableStatement cs = con.prepareCall(sql);
			cs.registerOutParameter(1, Types.BOOLEAN);
			cs.setInt(2, reimbur.getImbursmentId());
			cs.setInt(3, reimbur.getResolver());
			cs.setInt(4, reimbur.getStatusId());
			cs.execute();
			System.out.println(cs.getString(1));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Reimburstment> getRequestsForID(int idNum) {
		List<Reimburstment> reimbList = new ArrayList<>();

		try (Connection con = rDb.getDBConnection()) {

			String sql = "select * from reimbursementmain where reimb_submiter=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, idNum);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getDate(4) == null) {
					reimbList.add(new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
							rs.getDouble(2), rs.getDate(3).toString(), "------", rs.getString(5)));
				}else {
				reimbList.add(new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
						rs.getDouble(2), rs.getDate(3).toString(), rs.getDate(4).toString(), rs.getString(5)));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimbList;
	}

	@Override
	public List<Reimburstment> getRequestByStatus(){
		List<Reimburstment> reimbList = new ArrayList<>();

		try (Connection con = rDb.getDBConnection()) {

			String sql = "select * from reimbursementmain where reimb_statusid=1";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getDate(4) == null) {
					reimbList.add(new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
							rs.getDouble(2), rs.getDate(3).toString(), "------", rs.getString(5)));
				}else {
				reimbList.add(new Reimburstment(rs.getInt(1), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getInt(9),
						rs.getDouble(2), rs.getDate(3).toString(), rs.getDate(4).toString(), rs.getString(5)));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return reimbList;
	}

	@Override
	public Double getAvgRequest() {
		double result = 0;
		List<Double> tempList = new ArrayList<>();
		
		try(Connection con = rDb.getDBConnection()){
			String sql = "select avg(reimb_amnt) as Average from reimbursementmain";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				tempList.add(rs.getDouble(1));
			}
			result = tempList.get(0);
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Double getMaxRequest() {
		double result = 0;
		List<Double> tempList = new ArrayList<>();
		try(Connection con = rDb.getDBConnection()){
			String sql = "select reimb_submiter, max(reimb_amnt) from reimbursementmain group by reimb_submiter";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				int i = rs.getInt(1);
				tempList.add(rs.getDouble(2));
			}
			result = tempList.get(0);
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
