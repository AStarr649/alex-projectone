package com.example.dao;

import com.example.model.User;

public interface UserDAO {

	public User getUserByName(String fname, String lname);
	public User getUserById(int idNum);
	public User getUserLogin(String username);
}
