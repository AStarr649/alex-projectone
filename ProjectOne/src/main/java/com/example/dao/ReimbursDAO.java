package com.example.dao;

import java.util.List;

import com.example.model.Reimburstment;

public interface ReimbursDAO {

	void insertNewRequest(Reimburstment newReimburs);
	List<Reimburstment> getAllRequests();
	Reimburstment getRequestById(int imbursId);
	void updateRequestStatus(Reimburstment reimbur);
	public List<Reimburstment> getRequestsForID(int idNum);
	List<Reimburstment> getRequestByStatus();
	Double getAvgRequest();
	Double getMaxRequest();
}
