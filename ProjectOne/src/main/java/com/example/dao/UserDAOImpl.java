package com.example.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.model.User;

public class UserDAOImpl implements UserDAO {

	private ProjectDBConnection uDb;

	public UserDAOImpl() {
		// TODO Auto-generated constructor stub
	}

	public UserDAOImpl(ProjectDBConnection uDb) {
		super();
		this.uDb = uDb;
	}

	@Override
	public User getUserByName(String fname, String lname) {

		try (Connection con = uDb.getDBConnection()) {

			String sql = "select * from users where firstname=? and lastname=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, fname);
			ps.setString(2, lname);
			ResultSet rs = ps.executeQuery();
			User calledUser = null;

			while (rs.next()) {
				calledUser = new User(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5),
						rs.getString(6), rs.getString(7));
			}

			return calledUser;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public User getUserById(int idNum) {

		try (Connection con = uDb.getDBConnection()) {

			String sql = "select * from users where userid=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, idNum);
			ResultSet rs = ps.executeQuery();
			User calledUser = null;

			while (rs.next()) {
				calledUser = new User(rs.getInt(1), rs.getInt(7), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));
			}

			return calledUser;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User getUserLogin(String username) {
		try (Connection con = uDb.getDBConnection()) {

			String sql = "select * from users where username=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			User calledUser = null;

			while (rs.next()) {	
				calledUser = new User(rs.getInt(1), rs.getInt(7), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6));
			}

			return calledUser;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

}
