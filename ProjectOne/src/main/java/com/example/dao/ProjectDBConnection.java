package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProjectDBConnection {

	private final String URL = "jdbc:postgresql://starr-db-rev.ckvsjuohfewy.us-east-2.rds.amazonaws.com:5432/projectonedb";
	private final String USERNAME = "projectoneuser";
	private final String PASSWORD = "P4ssw0rd";
	
	public Connection getDBConnection() throws SQLException {
		return DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
}
