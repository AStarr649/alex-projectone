package com.example.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.example.model.Reimburstment;
import com.example.model.User;
import com.example.service.ReimburstService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.javalin.http.Handler;

public class ReimbursController {

	private ReimburstService rServ;
	ObjectMapper mapper = new ObjectMapper();
	
	public ReimbursController() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursController(ReimburstService rServ) {
		super();
		this.rServ = rServ;
	}
	
	public final Handler POSTREQUEST = (ctx) -> {
		@SuppressWarnings("unchecked")
		Map<String, String> bodyMap = (Map<String, String>) mapper.readValue(ctx.body(), HashMap.class);
		User user = (User)ctx.sessionAttribute("currentuser");
		Reimburstment request = new Reimburstment(Double.parseDouble(bodyMap.get("amount")), user.getUserid(), bodyMap.get("date"), bodyMap.get("comment"));
		rServ.newRequest(request);
		ctx.status(201);
		ctx.redirect("/html/home.html");
	};
	
	public final Handler GETALLREQUESTS = (ctx) -> {
		List<Reimburstment> fullList = rServ.retrieveAllReimbursments();
		ctx.status(200);
		ctx.json(fullList);
	};
	
	public final Handler GETREQUESTSPERID = (ctx) -> {
		int user = Integer.parseInt(ctx.pathParam("userid"));
		List<Reimburstment> userList = rServ.getAllRequestById(user);
		ctx.status(200);
		ctx.json(userList);
	};
	
	public final Handler GETREQUESTSPERSTATUS = (ctx) -> {
		List<Reimburstment> pendingList = rServ.getFilterRequest();
		ctx.status(200);
		ctx.json(pendingList);
	};
	
	public final Handler UPDATEREQUESTSTATUS = (ctx) -> {
		Reimburstment update = rServ.getReimbursRequestById(Integer.parseInt(ctx.sessionAttribute("requestID")));
		rServ.updateRequest(update);
		ctx.status(201);
		ctx.redirect("/html/home.html");
	};
	
	public final Handler GETAVGREQUEST = (ctx) -> {
		double result = rServ.getAvgRequest();
		ctx.status(201);
		ctx.json(result);
	};
	
	public final Handler GETMAXREQUEST = (ctx) -> {
		double result = rServ.getMaxRequest();
		ctx.status(201);
		ctx.json(result);
	};
}
