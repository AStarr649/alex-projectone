package com.example.controller;

import com.example.model.User;
import com.example.service.UserService;

import io.javalin.http.Handler;

public class UserController {

	private UserService uServ;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}
	
	public final Handler POSTLOGIN = (ctx) -> {
		
		User user = uServ.verifyUserLogin(ctx.formParam("username"), ctx.formParam("password"));
		if(user != null) {
			ctx.sessionAttribute("currentuser", user);
			ctx.redirect("/html/home.html");
		}else {
			ctx.redirect("/html/invalidlogin.html");
		}
	};
	
	public final Handler GETSESSUSER = (ctx) -> {
		
		if((User)ctx.sessionAttribute("currentuser") == null) {
			ctx.redirect("/html/invalidlogin.html");
		}else {
			User user = uServ.getUserById(((User)ctx.sessionAttribute("currentuser")).getUserid());
			ctx.json(user);
		}
	};
	
	public final Handler INVALIDATEUSER = (ctx) -> {
		
		ctx.sessionAttribute("currentuser", null);
		ctx.redirect("/html/login.html");
	};

	public final Handler GETNEWREQUEST = (ctx) -> {
		
		if((User)ctx.sessionAttribute("currentuser") == null) {
			ctx.redirect("/html/invalidlogin.html");
		}else {
			User user = uServ.getUserById(((User)ctx.sessionAttribute("currentuser")).getUserid());
			ctx.json(user);
			ctx.redirect("/html/newrequestform.html");
		}
	};
}
