package com.example.model;

import java.util.List;

public class User {

	private int userid, roleid;
	double amount = 0.0;
	private String username, password, fname, lname, email;
	private List<Reimburstment> userReimbList;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(int userid, String fname, String lname, double avg) {
		super();
		this.userid = userid;
		this.fname = fname;
		this.lname = lname;
		this.amount = avg;
	}
	
	public User(int roleid, String username, String password, String fname, String lname, String email) {
		super();
		this.roleid = roleid;
		this.username = username;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
	}

	public User(int userid, int roleid, String username, String password, String fname, String lname, String email) {
		super();
		this.userid = userid;
		this.roleid = roleid;
		this.username = username;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
	}

	public User(int userid, int roleid, String username, String password, String fname, String lname, String email,
			List<Reimburstment> userReimbList) {
		super();
		this.userid = userid;
		this.roleid = roleid;
		this.username = username;
		this.password = password;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.userReimbList = userReimbList;
	}

	public int getRoleid() {
		return roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getUserid() {
		return userid;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public List<Reimburstment> getUserReimbList() {
		return userReimbList;
	}

	public void setUserReimbList(List<Reimburstment> userReimbList) {
		this.userReimbList = userReimbList;
	}

	@Override
	public String toString() {
		return "User [userid=" + userid + ", roleid=" + roleid + ", username=" + username + ", password=" + password
				+ ", fname=" + fname + ", lname=" + lname + ", email=" + email + ", userReimbList=" + userReimbList
				+ "]";
	}

	
	
}
