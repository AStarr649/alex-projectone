package com.example.model;

public class Reimburstment {

	private int imbursmentId, submiter, resolver = 99, statusId = 1, typeId = 6;
	private double imbursAmnt;
	private String entryDate, resolvedDate = "", comment;
	
	/*
	 *  Memo to Self: Date format within PostgreSQL and Java to be readable MUST be in YYYY-MM-DD format, if another method to read and function comes along will mention or fix.
	 *  				For now, maintain the above format when handling the code portions and remember that format when transferring information to frontend.
	 */
	
	public Reimburstment() {
		// TODO Auto-generated constructor stub
	}

	public Reimburstment(int submiter, double amount) {
		super();
		this.submiter = submiter;
		this.imbursAmnt = amount;
	}
	
	public Reimburstment(double amount, int submiter, String entryDate, String comment) {
		super();
		this.imbursAmnt = amount;
		this.submiter = submiter;
		this.entryDate = entryDate;
		this.comment = comment;
	}
	
	public Reimburstment(double amount, int submiter, int typeId, String entryDate, String comment) {
		super();
		this.imbursAmnt = amount;
		this.submiter = submiter;
		this.typeId = typeId;
		this.entryDate = entryDate;
		this.comment = comment;
	}
	
	public Reimburstment(int submiter, int resolver, int statusId, int typeId, double imbursAmnt, String entryDate,
			String resolvedDate, String comment) {
		super();
		this.submiter = submiter;
		this.resolver = resolver;
		this.statusId = statusId;
		this.typeId = typeId;
		this.imbursAmnt = imbursAmnt;
		this.entryDate = entryDate;
		this.resolvedDate = resolvedDate;
		this.comment = comment;
	}

	public Reimburstment(int imbursmentId, int submiter, int resolver, int statusId, int typeId, double imbursAmnt,
			String entryDate, String resolvedDate, String comment) {
		super();
		this.imbursmentId = imbursmentId;
		this.submiter = submiter;
		this.resolver = resolver;
		this.statusId = statusId;
		this.typeId = typeId;
		this.imbursAmnt = imbursAmnt;
		this.entryDate = entryDate;
		this.resolvedDate = resolvedDate;
		this.comment = comment;
	}

	public int getSubmiter() {
		return submiter;
	}

	public void setSubmiter(int submiter) {
		this.submiter = submiter;
	}

	public int getResolver() {
		return resolver;
	}

	public void setResolver(int resolver) {
		this.resolver = resolver;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public double getImbursAmnt() {
		return imbursAmnt;
	}

	public void setImbursAmnt(double imbursAmnt) {
		this.imbursAmnt = imbursAmnt;
	}

	public String getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getResolvedDate() {
		return resolvedDate;
	}

	public void setResolvedDate(String resolvedDate) {
		this.resolvedDate = resolvedDate;
	}

	public int getImbursmentId() {
		return imbursmentId;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getComment() {
		return comment;
	}

	@Override
	public String toString() {
		return "Reimburstment [imbursmentId=" + imbursmentId + ", submiter=" + submiter + ", resolver=" + resolver
				+ ", statusId=" + statusId + ", typeId=" + typeId + ", imbursAmnt=" + imbursAmnt + ", entryDate="
				+ entryDate + ", resolvedDate=" + resolvedDate + ", comment=" + comment + "]";
	}
	
}
