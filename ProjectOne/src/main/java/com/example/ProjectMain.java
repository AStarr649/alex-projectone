package com.example;

import com.example.controller.ReimbursController;
import com.example.controller.UserController;
import com.example.dao.ProjectDBConnection;
import com.example.dao.ReimbursDAOImpl;
import com.example.dao.UserDAOImpl;
import com.example.service.ReimburstService;
import com.example.service.UserService;

import io.javalin.Javalin;

public class ProjectMain {

	public static void main(String[] args) {

		UserController uCon = new UserController(new UserService(new UserDAOImpl(new ProjectDBConnection())));
		ReimbursController rCon = new ReimbursController(new ReimburstService(new ReimbursDAOImpl(new ProjectDBConnection())));
		
		Javalin app = Javalin.create(config ->{
			config.enableCorsForAllOrigins();
			config.addStaticFiles("/frontend");
		});
		
		app.start(9070);

		app.post("/user/login", uCon.POSTLOGIN);
		app.get("/user/sesson", uCon.GETSESSUSER);
		app.post("/reimbursment", rCon.POSTREQUEST);
		app.get("/endsession", uCon.INVALIDATEUSER);
		app.get("/user/newrequest", uCon.GETNEWREQUEST);
		app.get("/user/:userid/reimbursements", rCon.GETREQUESTSPERID);
		app.get("/reimbursements", rCon.GETALLREQUESTS);
		app.get("/pending", rCon.GETREQUESTSPERSTATUS);
		app.post("reimbursement/update", rCon.UPDATEREQUESTSTATUS);
		app.get("/getavgrequest", rCon.GETAVGREQUEST);
		app.get("/getmaxrequest", rCon.GETMAXREQUEST);
		
		app.exception(NullPointerException.class, (e, ctx) -> {
			ctx.status(404);
			ctx.redirect("/html/invalidlogin.html");
		});
	}

}
