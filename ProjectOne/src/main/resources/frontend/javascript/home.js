/**
 *  Getting Session user
 */

window.onload = function () {
    getSessionUser();
}

let sessUser;
let filterstate = false;

function getSessionUser() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let user = JSON.parse(xhttp.responseText);
            sessUser = user;
            userInfo(user);
            loadTable();
        }
    }

    xhttp.open("GET", "http://localhost:9070/user/sesson");
    xhttp.send();
}

function userInfo(user) {
    document.getElementById("username").innerText = user.username;
    document.getElementById("userFName").innerText = user.fname;
    if (user.roleid == 1) {
        document.getElementById("role").innerText = "Employee";
        document.getElementById("newRequest").style.display = 'block';
    } else if (user.roleid == 2) {
        document.getElementById("role").innerText = "Manager";
        document.getElementById("filterPending").style.display = 'block';
        document.getElementById("showStats").style.display = 'block';
    }
}

document.getElementById("logoutlink").addEventListener('click', () => { invalidateSessionUser(); });

function invalidateSessionUser() {
    let xhttp = new XMLHttpRequest();

    xhttp.open("GET", "http://localhost:9070/endsession");
    xhttp.send();
}

document.getElementById("newRequest").onclick = function () {
    location.href = "/html/newrequestform.html";
}

document.getElementById("showStats").onclick = function () {
    location.href = "/html/requeststatus.html";
}

function loadTable() {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            if (document.getElementById("table").hasChildNodes == true) {
                clearTable();
            }
            buildTable(reimbArray);
        }
    }
    if (sessUser.roleid == 1) {
        xhttp.open("GET", "http://localhost:9070/user/" + sessUser.userid + "/reimbursements")
        xhttp.send();
    } else if (sessUser.roleid == 2 && filterstate == false) {
        xhttp.open("GET", "http://localhost:9070/reimbursements");
        xhttp.send();
    } else if (sessUser.roleid == 2 && filterstate == true) {
        filterToPendingRequests();
    }
}

document.getElementById("filterPending").addEventListener('click', () => {
    filterToPendingRequests();
    if (filterstate == false) {
        document.getElementById("filterPending").setAttribute("value", "Show All");

        filterstate = true;
    } else {
        document.getElementById("filterPending").setAttribute("value", "Show Pending");
        filterstate = false;
    }

});

function filterToPendingRequests() {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let reimbArray = JSON.parse(xhttp.responseText);
            clearTable();
            buildTable(reimbArray);
        }
    }
    if (filterstate == false) {
        xhttp.open("GET", "http://localhost:9070/pending");
        xhttp.send();
    } else if (filterstate == true) {
        xhttp.open("GET", "http://localhost:9070/reimbursements");
        xhttp.send();
    }

}

function clearTable() {
    document.getElementById("tableBody").innerHTML = "";
}

function approveDeny(rowNum) {
    let xhttp = new XMLHttpRequest();
    let choice = confirm("Do you wish to Approve or Deny this ERS Request?");
    if (choice == true) {
        let requestID = document.getElementById("sr" + rowNum);
        let statusNum = 2;
        let data = { requestID, statusNum };
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                console.log("success");
            }
        }
        xhttp.open("POST", "http://localhost:9070/reimbursement/update");
        xhttp.send(JSON.stringify(data));
    } else if (choice == false) {
        let requestID = document.getElementById("sr" + rowNum);
        let statusNum = 3;
        let data = { requestID, statusNum };
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                console.log("success");
            }
        }
        xhttp.open("POST", "http://localhost:9070/reimbursement/update");
        xhttp.send(JSON.stringify(data));
    }
}

function dummyFunction(i){
    console.log("button " + i + " pressed")
}

function buildTable(tableArry) {
    let rowNum = 0;
    for (let temp of tableArry) {
        var table = document.getElementById("tableBody");      
        var row = table.insertRow(rowNum);
        var sn = row.insertCell(0);
        sn.setAttribute("id", "sr" + rowNum);
        var amount = row.insertCell(1);
        var subDate = row.insertCell(2);
        var resolvDate = row.insertCell(3);
        var desc = row.insertCell(4);
        var submitter = row.insertCell(5);
        var resolvBy = row.insertCell(6);
        var status = row.insertCell(7);
        var typeid = row.insertCell(8);

        sn.innerText = temp.imbursmentId;
        amount.innerText = `$ ${temp.imbursAmnt}`;
        subDate.innerText = temp.entryDate;
        resolvDate.innerText = temp.resolvedDate;
        desc.innerText = temp.comment;
        submitter.innerText = temp.submiter;
        resolvBy.innerText = temp.resolver;
        if (resolvBy.innerText == '0') {
            resolvBy.innerText = "--";
        }
        status.innerText = temp.statusId;
        typeid.innerText = temp.typeId;
        switch (status.innerText) {
            case '1':
                status.innerText = "Pending";
                if (sessUser.userid == 2) {                      
                    var button = document.createElement("button");
                    button.setAttribute("id", `adButton`);
                    button.setAttribute("value", `ADBtn${rowNum}`);
                    button.textContent = "Approve/Deny";
                    row.append(button);
                }
                break;
            case '2':
                status.innerText = "Approved";
                break;
            case '3':
                status.innerText = "Denied";
                break;
            default:
                status.innerText = "----";
                break;
        }
        switch (typeid.innerText) {
            case '1':
                typeid.innerText = "Travel";
                break;
            case '2':
                typeid.innerText = "Food";
                break;
            case '3':
                typeid.innerText = "Lodging";
                break;
            case '4':
                typeid.innerText = "Training";
                break;
            case '5':
                typeid.innerText = "Other";
                break;
            case '7':
                typeid.innerText = "Rejected";
                break;
            default:
                typeid.innerText = "Unassigned";
                break;

        }
        rowNum++;
    }
    const btns = document.querySelectorAll('#adButton');
    for(let i = 0; i < btns.length; i++){
        btns[i].addEventListener('click', function(e){
            var temp = document.getElementById("adButton").getAttribute("value");
            dummyFunction(temp);
        })
    }
}