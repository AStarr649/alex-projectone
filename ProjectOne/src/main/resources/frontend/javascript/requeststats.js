/**
 * Shows Reimbursement Stats
 */
 window.onload = function () {
    getSessionUser();
    loadAvgRequest();
    loadMaxRequest();
}

function getSessionUser() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let user = JSON.parse(xhttp.responseText);
            sessUser = user;
            userInfo(user);
        }
    }

    xhttp.open("GET", "http://localhost:9070/user/sesson");
    xhttp.send();
}

function userInfo(user) { 
    document.getElementById("userFName").innerText = "Hello " + user.fname + "\nHere are the current request stats.";
    
}

function loadAvgRequest(){
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 201){
            let average = JSON.parse(xhttp.responseText);
            document.getElementById("avgRequest").innerText = `Current Average Reimbursement Amount is: $ ${average}`;
        }
    }
    xhttp.open("GET", "http://localhost:9070/getavgrequest");
    xhttp.send();
}

function loadMaxRequest(){
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if(xhttp.readyState == 4 && xhttp.status == 201){
            let most = JSON.parse(xhttp.responseText);
            document.getElementById("maxRequest").innerText = `Current Highest Reimbursement Request is: $ ${most}`;
        }
    }
    xhttp.open("GET", "http://localhost:9070/getmaxrequest");
    xhttp.send();
}

document.getElementById("homebtn").onclick = function () {
    location.href = "/html/home.html";
}
