/**
 *  Creating a new Reimbursment Request
 */

 window.onload=function(){
    getSessionUser();
}

let sessUser;

function getSessionUser() {
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            let user = JSON.parse(xhttp.responseText);
            sessUser = user;
            //console.log(user);
            userInfo(user);
        }
    }

    xhttp.open("GET", "http://localhost:9070/user/sesson");
    xhttp.send();
}

function userInfo(user){
    document.getElementById("firstname").innerText=sessUser.fname;
	document.getElementById("lastname").innerText=sessUser.lname;
    
}

document.getElementById("newSubmit").onclick = function() {
    let xhttp = new XMLHttpRequest();
    let date = new Date();
    let amount = document.getElementById("requestAmount").value;
    let comment = document.getElementById("description").value;
    let userNum = sessUser.userid;

    let request = JSON.stringify({amount, userNum, date, comment})

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 201) {
            console.log("file trasfered")
        }
    }

    xhttp.open("POST", "http://localhost:9070/reimbursment");
    xhttp.send(request);   
}

document.getElementById("cancel").onclick = function () {
    location.href = "/html/home.html";
}

