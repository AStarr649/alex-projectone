package com.example.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.example.dao.ProjectDBConnection;
import com.example.dao.ReimbursDAOImpl;
import com.example.model.Reimburstment;

public class ReimbDAOTest {

	@Mock
	private ProjectDBConnection rDb;
	@Mock
	private Connection con;
	@Mock
	private PreparedStatement ps;
	@Mock
	private CallableStatement cs;
	@Mock
	private ResultSet rs;
	
	private ReimbursDAOImpl rDao;
	private Reimburstment reimburs;
	private List<Reimburstment> reimbList;
	
	@BeforeEach
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);		
		rDao = new ReimbursDAOImpl(rDb);
		when(rDb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
		when(cs.execute()).thenReturn(true);
		when(cs.getString(isA(Integer.class))).thenReturn("Test Success");
		reimburs = new Reimburstment(8, 4, 12, 1, 4, 89.48, "2011-04-14", "2011-04-24", "Wakka wakka");
		reimbList = Arrays.asList(reimburs);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(reimburs.getImbursmentId());
		when(rs.getDouble(2)).thenReturn(reimburs.getImbursAmnt());
		when(rs.getDate(3)).thenReturn(Date.valueOf(reimburs.getEntryDate()));
		when(rs.getDate(4)).thenReturn(Date.valueOf(reimburs.getResolvedDate()));
		when(rs.getString(5)).thenReturn(reimburs.getComment());
		when(rs.getInt(6)).thenReturn(reimburs.getSubmiter());
		when(rs.getInt(7)).thenReturn(reimburs.getResolver());
		when(rs.getInt(8)).thenReturn(reimburs.getStatusId());
		when(rs.getInt(9)).thenReturn(reimburs.getTypeId());
	}
	
	@Test
	public void insertNewRequestSuccess() throws SQLException {
		rDao.insertNewRequest(reimburs);
		verify(cs, times(1)).execute();
	}
	
	@Test
	public void getAllRequestsSucces() throws SQLException {
		List<Reimburstment> testList = rDao.getAllRequests();
		assertEquals(reimburs.getImbursmentId(), testList.get(0).getImbursmentId());
		assertEquals(reimburs.getComment(), testList.get(0).getComment());
		assertEquals(reimburs.getEntryDate(), testList.get(0).getEntryDate());
		assertEquals(reimburs.getImbursAmnt(), testList.get(0).getImbursAmnt());
		assertEquals(reimburs.getResolvedDate(), testList.get(0).getResolvedDate());
		assertEquals(reimburs.getResolver(), testList.get(0).getResolver());
		assertEquals(reimburs.getStatusId(), testList.get(0).getStatusId());
		assertEquals(reimburs.getSubmiter(), testList.get(0).getSubmiter());
		assertEquals(reimburs.getTypeId(), testList.get(0).getTypeId());
	}
	
	@Test
	public void getRequestByIdSuccess() throws SQLException {
		Reimburstment testImburs = rDao.getRequestById(5);
		assertEquals(reimburs.getImbursmentId(), testImburs.getImbursmentId());
		assertEquals(reimburs.getComment(), testImburs.getComment());
		assertEquals(reimburs.getEntryDate(), testImburs.getEntryDate());
		assertEquals(reimburs.getImbursAmnt(), testImburs.getImbursAmnt());
		assertEquals(reimburs.getResolvedDate(), testImburs.getResolvedDate());
		assertEquals(reimburs.getResolver(), testImburs.getResolver());
		assertEquals(reimburs.getStatusId(), testImburs.getStatusId());
		assertEquals(reimburs.getSubmiter(), testImburs.getSubmiter());
		assertEquals(reimburs.getTypeId(), testImburs.getTypeId());
	}
	
	@Test
	public void updateRequestSuccess() throws SQLException {
		rDao.updateRequestStatus(reimburs);
		verify(cs, times(1)).execute();
	}
}
