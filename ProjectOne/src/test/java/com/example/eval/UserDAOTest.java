package com.example.eval;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.dao.ProjectDBConnection;
import com.example.dao.UserDAOImpl;
import com.example.model.User;

public class UserDAOTest {

	@Mock
	private ProjectDBConnection pDb;
	@Mock
	private Connection con;
	@Mock
	private PreparedStatement ps;
	@Mock
	private ResultSet rs;
	
	private UserDAOImpl uDao;
	private User user;
	private List<User> userList;
	
	@BeforeEach
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		uDao = new UserDAOImpl(pDb);
		when(pDb.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(isA(String.class))).thenReturn(ps);
		//when(con.prepareCall(isA(String.class))).thenReturn(cs);
		when(ps.executeQuery()).thenReturn(rs);
		when(ps.execute()).thenReturn(true);
//		when(cs.execute()).thenReturn(true);
//		when(cs.getString(isA(Integer.class))).thenReturn("Test Success");
		user = new User(6, 2, "dangBobby", "password", "Bobby", "Hill", "hill.propane@arlen.com");
		userList = Arrays.asList(user);
		when(rs.next()).thenReturn(true).thenReturn(false);
		when(rs.getInt(1)).thenReturn(user.getUserid());
		when(rs.getInt(2)).thenReturn(user.getRoleid());
		when(rs.getString(3)).thenReturn(user.getUsername());
		when(rs.getString(4)).thenReturn(user.getPassword());
		when(rs.getString(5)).thenReturn(user.getFname());
		when(rs.getString(6)).thenReturn(user.getLname());
		when(rs.getString(7)).thenReturn(user.getEmail());
	}
	
	@Test
	public void getUserByNameSuccess() throws SQLException {
		User testUser = uDao.getUserByName("Richard", "Simmons");
		assertEquals(user.getRoleid(), testUser.getRoleid());
		assertEquals(user.getUserid(), testUser.getUserid());
		assertEquals(user.getFname(), testUser.getFname());
		assertEquals(user.getLname(), testUser.getLname());
		assertEquals(user.getUsername(), testUser.getUsername());
		assertEquals(user.getPassword(), testUser.getPassword());
		assertEquals(user.getEmail(), testUser.getEmail());
	}
	
	@Test
	public void getUserByIdSuccess() throws SQLException {
		User testUser = uDao.getUserById(7);
		assertEquals(user.getRoleid(), testUser.getRoleid());
		assertEquals(user.getUserid(), testUser.getUserid());
		assertEquals(user.getFname(), testUser.getFname());
		assertEquals(user.getLname(), testUser.getLname());
		assertEquals(user.getUsername(), testUser.getUsername());
		assertEquals(user.getPassword(), testUser.getPassword());
		assertEquals(user.getEmail(), testUser.getEmail());
	}
	
	@Test
	public void getUserByLoginSuccess() throws SQLException {
		User testUser = uDao.getUserLogin("bartMan");
		assertEquals(user.getRoleid(), testUser.getRoleid());
		assertEquals(user.getUserid(), testUser.getUserid());
		assertEquals(user.getFname(), testUser.getFname());
		assertEquals(user.getLname(), testUser.getLname());
		assertEquals(user.getUsername(), testUser.getUsername());
		assertEquals(user.getPassword(), testUser.getPassword());
		assertEquals(user.getEmail(), testUser.getEmail());
	}
}
